// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef DIAGNOSTICS_TELEM_TELEMETRY_GROUP_ENUM_H_
#define DIAGNOSTICS_TELEM_TELEMETRY_GROUP_ENUM_H_

namespace diagnostics {

// Enumerates each of the groups of related telemetry items that
// can be requested from libtelem.
enum class TelemetryGroupEnum { kMemory };

}  // namespace diagnostics

#endif  // DIAGNOSTICS_TELEM_TELEMETRY_GROUP_ENUM_H_
