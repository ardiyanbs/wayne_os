# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
from fuzzy_check import FuzzyCheck
from validators import *

# originally generated gestures:
# FlingStop
#   FlingStop
#   ButtonDown(1)
#   Motion d=1189 x=1152 y=280 r=19.89
#   Motion d=1 x=0 y=1 r=0.00
#   ButtonUp(1)

def Validate(raw, events, gestures):
  fuzzy = FuzzyCheck()
  fuzzy.expected = [
    ButtonDownValidator(1),
    MotionValidator("== 1190 ~ 1000"),
    ButtonUpValidator(1),
  ]
  fuzzy.unexpected = [
    MotionValidator("== 0 ~ 10"),
    FlingStopValidator("<10"),
  ]
  return fuzzy.Check(gestures)
