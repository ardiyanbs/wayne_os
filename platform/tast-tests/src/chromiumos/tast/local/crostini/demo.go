// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package crostini

const (
	// X11DemoAppPath is a path (in the container) to an application used for verifying simple X11 functionality.
	X11DemoAppPath = "/opt/google/cros-containers/bin/x11_demo"

	// WaylandDemoAppPath is a path (in the container) to an application used for verifying simple Wayland functionality.
	WaylandDemoAppPath = "/opt/google/cros-containers/bin/wayland_demo"
)
