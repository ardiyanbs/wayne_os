# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "Simple Watchdog daemon"
author        "chromium-os-dev@chromium.org"

start on started boot-services
stop on stopping boot-services
respawn
expect fork

env minijail_common_args=" \
  -I -p -e -l -r -v -n --uts \
  --profile minimalistic-mountns -b /dev/watchdog \
  -u watchdog -g watchdog \
  -- /usr/sbin/daisydog"

pre-start script
  # We need the check command to *not* background/exit early, so do not pass
  # in the -i flag to it.  This way we can capture its output and exit status.
  check_cmd() {
    /sbin/minijail0 ${minijail_common_args} -c
  }

  # Run in "check" mode to determine if the last reset was because
  # of a watchdog timeout.
  if ! output="$(check_cmd 2>&1)"; then
    # If we get a failure here, assume the watchdog is broken (or doesn't
    # exist as in a VM), and exit (which halts the init script entirely).
    logger -t "${UPSTART_JOB}" -p daemon.error \
      "error: watchdog startup failed: ${output}"
    # Log a metric so we can keep tabs on when this happens in real life.
    metrics_client -v Watchdog.StartupFailed
    exit 1
  fi

  case "${output}" in
  *watchdog-timeout*)
    metrics_client -v HwWatchdogReboot
    ;;
  esac
end script

exec chrt -r 99 /sbin/minijail0 -i ${minijail_common_args}
