From cc2bebe039374d6f0c8251ac322f8f7148d85950 Mon Sep 17 00:00:00 2001
From: Bill Wendling <isanbard@gmail.com>
Date: Wed, 14 Aug 2019 16:44:07 +0000
Subject: [PATCH] Ignore indirect branches from callbr.

Summary:
We can't speculate around indirect branches: indirectbr and invoke. The
callbr instruction needs to be included here.

Reviewers: nickdesaulniers, manojgupta, chandlerc

Reviewed By: chandlerc

Subscribers: llvm-commits

Tags: #llvm

Differential Revision: https://reviews.llvm.org/D66200

llvm-svn: 368873
---
 .../Transforms/Scalar/SpeculateAroundPHIs.cpp |  6 ++-
 .../Transforms/SpeculateAroundPHIs/pr42991.ll | 44 +++++++++++++++++++
 2 files changed, 48 insertions(+), 2 deletions(-)
 create mode 100644 llvm/test/Transforms/SpeculateAroundPHIs/pr42991.ll

diff --git a/llvm/lib/Transforms/Scalar/SpeculateAroundPHIs.cpp b/llvm/lib/Transforms/Scalar/SpeculateAroundPHIs.cpp
index c13fb3e0451..e6db11f47ea 100644
--- a/llvm/lib/Transforms/Scalar/SpeculateAroundPHIs.cpp
+++ b/llvm/lib/Transforms/Scalar/SpeculateAroundPHIs.cpp
@@ -777,8 +777,10 @@ static bool tryToSpeculatePHIs(SmallVectorImpl<PHINode *> &PNs,
     // speculation if the predecessor is an invoke. This doesn't seem
     // fundamental and we should probably be splitting critical edges
     // differently.
-    if (isa<IndirectBrInst>(PredBB->getTerminator()) ||
-        isa<InvokeInst>(PredBB->getTerminator())) {
+    const auto *TermInst = PredBB->getTerminator();
+    if (isa<IndirectBrInst>(TermInst) ||
+        isa<InvokeInst>(TermInst) ||
+        isa<CallBrInst>(TermInst)) {
       LLVM_DEBUG(dbgs() << "  Invalid: predecessor terminator: "
                         << PredBB->getName() << "\n");
       return false;
diff --git a/llvm/test/Transforms/SpeculateAroundPHIs/pr42991.ll b/llvm/test/Transforms/SpeculateAroundPHIs/pr42991.ll
new file mode 100644
index 00000000000..4aef689f53d
--- /dev/null
+++ b/llvm/test/Transforms/SpeculateAroundPHIs/pr42991.ll
@@ -0,0 +1,44 @@
+; RUN: opt -S -passes=spec-phis %s
+
+; This testcase crashes during the speculate around PHIs pass. The pass however
+; results in no changes.
+
+define i32 @test1() {
+entry:
+  callbr void asm sideeffect "", "X,X,~{dirflag},~{fpsr},~{flags}"(i8* blockaddress(@test1, %return), i8* blockaddress(@test1, %f))
+          to label %asm.fallthrough [label %return, label %f]
+
+asm.fallthrough:
+  br label %return
+
+f:
+  br label %return
+
+return:
+  %retval.0 = phi i32 [ 0, %f ], [ 1, %asm.fallthrough ], [ 1, %entry ]
+  ret i32 %retval.0
+}
+
+define void @test2() {
+entry:
+  br label %tailrecurse
+
+tailrecurse:
+  %call = tail call i32 @test3()
+  %tobool1 = icmp eq i32 %call, 0
+  callbr void asm sideeffect "", "X,X,~{dirflag},~{fpsr},~{flags}"(i8* blockaddress(@test2, %test1.exit), i8* blockaddress(@test2, %f.i))
+          to label %if.end6 [label %test1.exit, label %f.i]
+
+f.i:
+  br label %test1.exit
+
+test1.exit:
+  %retval.0.i = phi i1 [ false, %f.i ], [ true, %tailrecurse ]
+  %brmerge = or i1 %tobool1, %retval.0.i
+  br i1 %brmerge, label %if.end6, label %tailrecurse
+
+if.end6:
+  ret void
+}
+
+declare i32 @test3()
-- 
2.23.0.rc1.153.gdeed80330f-goog

