From 0503add6dab545600b99c89753fcc7cb5bd15fc8 Mon Sep 17 00:00:00 2001
From: Francis Visoiu Mistrih <francisvm@yahoo.com>
Date: Thu, 25 Jul 2019 22:23:48 +0000
Subject: [PATCH] [CodeGen] Don't resolve the stack protector frame accesses
 until PEI

Currently, stack protector loads and stores are resolved during
LocalStackSlotAllocation (if the pass needs to run). When this is the
case, the base register assigned to the frame access is going to be one
of the vregs created during LocalStackSlotAllocation. This means that we
are keeping a pointer to the stack protector slot, and we're using this
pointer to load and store to it.

In case register pressure goes up, we may end up spilling this pointer
to the stack, which can be a security concern.

Instead, leave it to PEI to resolve the frame accesses. In order to do
that, we make all stack protector accesses go through frame index
operands, then PEI will resolve this using an offset from sp/fp/bp.

Differential Revision: https://reviews.llvm.org/D64759

llvm-svn: 367068
---
 llvm/lib/CodeGen/LocalStackSlotAllocation.cpp |  8 ++++
 .../CodeGen/AArch64/stack-guard-reassign.ll   |  7 ++--
 llvm/test/CodeGen/ARM/stack-guard-reassign.ll |  7 ++--
 .../CodeGen/PowerPC/stack-guard-reassign.ll   | 11 +++---
 llvm/test/CodeGen/Thumb/stack_guard_remat.ll  | 39 +++++++++++++------
 5 files changed, 46 insertions(+), 26 deletions(-)

diff --git a/llvm/lib/CodeGen/LocalStackSlotAllocation.cpp b/llvm/lib/CodeGen/LocalStackSlotAllocation.cpp
index b14d76a585f..69ceacd0c18 100644
--- a/llvm/lib/CodeGen/LocalStackSlotAllocation.cpp
+++ b/llvm/lib/CodeGen/LocalStackSlotAllocation.cpp
@@ -351,6 +351,14 @@ bool LocalStackSlotPass::insertFrameReferenceRegisters(MachineFunction &Fn) {
     assert(MFI.isObjectPreAllocated(FrameIdx) &&
            "Only pre-allocated locals expected!");
 
+    // We need to keep the references to the stack protector slot through frame
+    // index operands so that it gets resolved by PEI rather than this pass.
+    // This avoids accesses to the stack protector though virtual base
+    // registers, and forces PEI to address it using fp/sp/bp.
+    if (MFI.hasStackProtectorIndex() &&
+        FrameIdx == MFI.getStackProtectorIndex())
+      continue;
+
     LLVM_DEBUG(dbgs() << "Considering: " << MI);
 
     unsigned idx = 0;
diff --git a/llvm/test/CodeGen/AArch64/stack-guard-reassign.ll b/llvm/test/CodeGen/AArch64/stack-guard-reassign.ll
index 632774c970e..a4838404733 100644
--- a/llvm/test/CodeGen/AArch64/stack-guard-reassign.ll
+++ b/llvm/test/CodeGen/AArch64/stack-guard-reassign.ll
@@ -3,7 +3,6 @@
 ; Verify that the offset assigned to the stack protector is at the top of the
 ; frame, covering the locals.
 ; CHECK-LABEL: fn:
-; CHECK:      sub x8, x29, #24
-; CHECK-NEXT: adrp x9, __stack_chk_guard
-; CHECK-NEXT: ldr x9, [x9, :lo12:__stack_chk_guard]
-; CHECK-NEXT: str x9, [x8]
+; CHECK:      adrp x8, __stack_chk_guard
+; CHECK-NEXT: ldr x8, [x8, :lo12:__stack_chk_guard]
+; CHECK-NEXT: stur x8, [x29, #-24]
diff --git a/llvm/test/CodeGen/ARM/stack-guard-reassign.ll b/llvm/test/CodeGen/ARM/stack-guard-reassign.ll
index 2ce1d1588a4..02ee9c067f2 100644
--- a/llvm/test/CodeGen/ARM/stack-guard-reassign.ll
+++ b/llvm/test/CodeGen/ARM/stack-guard-reassign.ll
@@ -5,10 +5,9 @@
 ; CHECK-LABEL: fn:
 ; CHECK:      sub sp, sp, #32
 ; CHECK-NEXT: sub sp, sp, #65536
+; CHECK-NEXT: ldr r1, .LCPI0_0
+; CHECK-NEXT: ldr r2, [r1]
 ; CHECK-NEXT: add lr, sp, #65536
-; CHECK-NEXT: add r1, lr, #28
-; CHECK-NEXT: ldr r2, .LCPI0_0
-; CHECK-NEXT: ldr r3, [r2]
-; CHECK-NEXT: str r3, [r1]
+; CHECK-NEXT: str r2, [lr, #28]
 ; CHECK: .LCPI0_0:
 ; CHECK-NEXT: .long __stack_chk_guard
diff --git a/llvm/test/CodeGen/PowerPC/stack-guard-reassign.ll b/llvm/test/CodeGen/PowerPC/stack-guard-reassign.ll
index 8128b63d598..e20a8cd11bb 100644
--- a/llvm/test/CodeGen/PowerPC/stack-guard-reassign.ll
+++ b/llvm/test/CodeGen/PowerPC/stack-guard-reassign.ll
@@ -9,9 +9,8 @@
 ; CHECK-NEXT: ori 0, 0, 65488
 ; CHECK-NEXT: stwux 1, 1, 0
 ; CHECK-NEXT: subf 0, 0, 1
-; CHECK-NEXT: lis 4, 1
-; CHECK-NEXT: ori 4, 4, 44
-; CHECK-NEXT: add 4, 1, 4
-; CHECK-NEXT: lis 5, __stack_chk_guard@ha
-; CHECK-NEXT: lwz 6, __stack_chk_guard@l(5)
-; CHECK-NEXT: stw 6, 0(4)
+; CHECK-NEXT: lis 4, __stack_chk_guard@ha
+; CHECK-NEXT: lwz 5, __stack_chk_guard@l(4)
+; CHECK-NEXT: lis 6, 1
+; CHECK-NEXT: ori 6, 6, 44
+; CHECK-NEXT: stwx 5, 1, 6
diff --git a/llvm/test/CodeGen/Thumb/stack_guard_remat.ll b/llvm/test/CodeGen/Thumb/stack_guard_remat.ll
index 294c6a6bd45..9b199ef407b 100644
--- a/llvm/test/CodeGen/Thumb/stack_guard_remat.ll
+++ b/llvm/test/CodeGen/Thumb/stack_guard_remat.ll
@@ -2,25 +2,40 @@
 ; RUN: llc < %s -mtriple=thumb-apple-darwin -relocation-model=static -no-integrated-as | FileCheck %s -check-prefix=NO-PIC  -check-prefix=STATIC
 ; RUN: llc < %s -mtriple=thumb-apple-darwin -relocation-model=dynamic-no-pic -no-integrated-as | FileCheck %s  -check-prefix=NO-PIC -check-prefix=DYNAMIC-NO-PIC
 
-;PIC:   foo2
-;PIC:   ldr [[R0:r[0-9]+]], [[LABEL0:LCPI[0-9_]+]]
-;PIC: [[LABEL1:LPC[0-9_]+]]:
-;PIC:   add [[R0]], pc
-;PIC:   ldr [[R1:r[0-9]+]], {{\[}}[[R0]]{{\]}}
-;PIC:   ldr [[R1:r[0-9]+]], {{\[}}[[R1]]{{\]}}
-
-;PIC:      [[LABEL0]]:
+;PIC:        foo2
+;PIC:        ldr [[SAVED_GUARD:r[0-9]+]], [[GUARD_STACK_OFFSET:LCPI[0-9_]+]]
+;PIC-NEXT:   add [[SAVED_GUARD]], sp
+;PIC-NEXT:   ldr [[SAVED_GUARD]], {{\[}}[[SAVED_GUARD]]{{\]}}
+;PIC-NEXT:   ldr [[ORIGINAL_GUARD:r[0-9]+]], [[ORIGINAL_GUARD_LABEL:LCPI[0-9_]+]]
+;PIC-NEXT: [[LABEL1:LPC[0-9_]+]]:
+;PIC-NEXT:   add [[ORIGINAL_GUARD]], pc
+;PIC-NEXT:   ldr [[ORIGINAL_GUARD]], {{\[}}[[ORIGINAL_GUARD]]{{\]}}
+;PIC-NEXT:   ldr [[ORIGINAL_GUARD]], {{\[}}[[ORIGINAL_GUARD]]{{\]}}
+;PIC-NEXT:   subs {{r[0-9]+}}, [[ORIGINAL_GUARD]], [[SAVED_GUARD]]
+
+;PIC:      [[GUARD_STACK_OFFSET]]:
+;PIC-NEXT:   .long 1028
+;PIC:      [[ORIGINAL_GUARD_LABEL]]:
 ;PIC-NEXT:   .long L___stack_chk_guard$non_lazy_ptr-([[LABEL1]]+4)
 
 ;NO-PIC:   foo2
-;NO-PIC:   ldr [[R0:r[0-9]+]], [[LABEL0:LCPI[0-9_]+]]
+;NO-PIC:                ldr [[SAVED_GUARD:r[0-9]+]], [[GUARD_STACK_OFFSET:LCPI[0-9_]+]]
+;NO-PIC-NEXT:           add [[SAVED_GUARD]], sp
+;NO-PIC-NEXT:           ldr [[SAVED_GUARD]], {{\[}}[[SAVED_GUARD]]{{\]}}
+;NO-PIC-NEXT:           ldr [[ORIGINAL_GUARD:r[0-9]+]], [[ORIGINAL_GUARD_LABEL:LCPI[0-9_]+]]
 ;NO-PIC-NOT: LPC
-;NO-PIC:   ldr {{r[0-9]+}}, {{\[}}[[R0]]{{\]}}
+;NO-PIC-NEXT:           ldr [[ORIGINAL_GUARD]], {{\[}}[[ORIGINAL_GUARD]]{{\]}}
+;DYNAMIC-NO-PIC-NEXT:   ldr [[ORIGINAL_GUARD]], {{\[}}[[ORIGINAL_GUARD]]{{\]}}
+;NO-PIC-NEXT:           subs {{r[0-9]+}}, [[ORIGINAL_GUARD]], [[SAVED_GUARD]]
 
-;STATIC:      [[LABEL0]]:
+;STATIC:      [[GUARD_STACK_OFFSET]]:
+;STATIC-NEXT:   .long 1028
+;STATIC:      [[ORIGINAL_GUARD_LABEL]]:
 ;STATIC-NEXT:   .long ___stack_chk_guard
 
-;DYNAMIC-NO-PIC:      [[LABEL0]]:
+;DYNAMIC-NO-PIC:      [[GUARD_STACK_OFFSET]]:
+;DYNAMIC-NO-PIC-NEXT:   .long 1028
+;DYNAMIC-NO-PIC:      [[ORIGINAL_GUARD_LABEL]]:
 ;DYNAMIC-NO-PIC-NEXT:   .long L___stack_chk_guard$non_lazy_ptr
 
 ; Function Attrs: nounwind ssp
-- 
2.22.0.709.g102302147b-goog

