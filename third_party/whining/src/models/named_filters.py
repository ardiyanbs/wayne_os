# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.


"""A filter is a set of criteria applied against test result queries.

This file contains an initial set of filters (until they are somehow relocated
into the db) and the common access method for using them: assemble_filters().

Background:

Each request to the dashboard starts with a predefined named filter.
The filter is then modified with some additional parameters from the URL
or query string. This file is intended to keep the definitions of named
named filters.

Each filter is a dict with some of the keys listed in KNOWN_QVARS.

The value associated with each key is a list of things to include.

Example query strings:
    ?suites=wificell,network3g
    ?tests=power_Resume,power_UiResume&releases=22
    ?tests=security_.*
    ?builds=R25-3417.0.0

Note: avoid creating filters with 'tests' but without 'suites'. For such filters
missing suites display won't work correctly and they result in SQL queries that
can't use good indexes on the main tables.

Wildcards:
You can use wildcards for specifying some values, usually test_names. See
security filter for example. MySQL REGEXP uses the usual regexp syntax, but it's
not case sensitive and does a search rather than match. If you want to match
something at the beginning or the end of a string, use ^ and $ chars, e.g:
'^platform.*' or '.*some_tag$'.
Use of wildcards for suite names is not allowed because it results in long
running SQL queries.
"""


# KNOWN_QVARS are those that are accepted as query parameters on the url.
# Not all of these will get translated into db query field parameters.
KNOWN_QVARS = [
    'builds',
    'days_back',
    'embedded',
    'modems',
    'platforms',
    'releases',
    'hide_experimental',
    'hide_missing',
    'include_invalid',
    'suites',
    'tests',
    'test_ids',
    'hostnames',
    'show_faft_view',
    ]

# TODO(jrbarnette):  Remove 'bvt' from the list below once it's no
# longer in use on any branch.
BVT_SUITES = ['bvt', 'bvt-inline', 'bvt-cq', 'bvt-perbuild']


NAMED_FILTERS = {
    #--------------------------------------------------------------------------
    # system filters
    'unfiltered': {},

    'lookups': {'days_back': 365},

    'health': {'days_back': 7, 'releases': ['tot']},

    #--------------------------------------------------------------------------
    # suite/area filters
    '3g': {
        'suites': ['network3g',
                   'network3g_att',
                   'network3g_tmobile',
                   'network3g_verizon',
                   ],
        'releases': ['tot'],
        'days_back': 7,
        },

    'audio': {
        'suites': ['audio'],
        'releases': ['tot'],
        'days_back': 7,
        },

    'bluetooth': {
        'suites': ['bluetooth_qualification',
                   'bluetooth_sanity',
                   ],
        'releases': ['tot'],
        'days_back': 7,
        },

    'browsertests': {
        'suites': ['browsertests'],
        'releases': ['tot'],
        'days_back': 1,
        },

    'bvt': {
        'suites': BVT_SUITES,
        'releases': ['tot'],
        'days_back': 3,
        },

    # This filter is mainly used for daily monitoring of per build CTS tests on
    # limited boards.
    'cts_perbuild': {
        'suites': ['arc-cts-perbuild'],
        'releases': ['tot'],
        'days_back': 15,
        },

    # This filter is mainly used for daily monitoring of CTS tests on ToT on all
    # boards.
    'cts_tot': {
        'suites': ['arc-cts'],
        'releases': ['tot'],
        'days_back': 15,
        },

    # This filter is mainly used for daily monitoring of CTS tests on the beta
    # channel.
    'cts_beta': {
        'suites': ['arc-cts'],
        'releases': ['tot-1'],
        'days_back': 15,
        },

    # This filter is mainly used for daily monitoring of CTS tests on the stable
    # channel, and different than suite:arc-cts-stable which is aimed for stable
    # channel build qualification.
    'cts_stable': {
        'suites': ['arc-cts'],
        'releases': ['tot-2'],
        'days_back': 15,
        },

    # This filter is mainly used for daily monitoring of per build GTS tests on
    # limited boards.
    'gts_perbuild': {
        'suites': ['arc-gts-perbuild'],
        'releases': ['tot'],
        'days_back': 15,
        },

    # This filter is mainly used for daily monitoring of GTS tests on ToT on all
    # boards.
    'gts_tot': {
        'suites': ['arc-gts'],
        'releases': ['tot'],
        'days_back': 15,
        },

    'bvt-cq': {
        'suites': ['bvt-cq'],
        'releases': ['tot'],
        'days_back': 15,
        },

    'cheets': {
        'suites': ['arc-bvt-cq'],
        'tests': ['^cheets_.*'],
        'release': ['tot'],
        'days_back': 7,
        },

    'graphics': {
        'suites': BVT_SUITES + [
            'graphics',
            'graphicsGLES',
            'graphics_per-build',
            'graphics_per-day',
            'graphics_per-week',
            'hwqual',
            'kernel_per-build_regression',
            'perf_v2',
            'qav',
            ],
        'tests': [
            '^graphics_.*',
            '^telemetry_GpuTests.*',
            '^video_VideoDecodeAccelerator.*',
            ],
        'releases': ['tot'],
        'days_back': 7,
        },

    'graphics_bvt': {
        'suites': BVT_SUITES,
        'releases': ['tot'],
        'tests': ['^graphics_.*'],
        'days_back': 7,
        },

    'graphics_cts': {
        'tests': [
            'cheets_CTS.android.nativeopengl',
            'cheets_CTS.android.dpi',
            'cheets_CTS.android.graphics.*',
            'cheets_CTS.android.nativeopengl',
            'cheets_CTS.android.openglperf',
            'cheets_CTS.android.renderscript.*',
            'cheets_CTS.android.textureview',
            'cheets_CTS.com.android.cts.jank.*',
            'cheets_CTS.com.android.cts.opengl',
            'cheets_CTS.com.drawelements.deqp.*',
        ],
        'releases': ['tot'],
        'days_back': 7,
     },

    'hwqual': {
        'suites': ['hwqual'],
        'releases': ['tot'],
        'days_back': 7,
        },

    'kernel': {
        'suites': ['kernel_per-build_regression',
                   'kernel_per-build_benchmarks',
                   ],
        'releases': ['tot'],
        'days_back': 3,
        },

    'kernel_bvt' : {
        'suites': BVT_SUITES,
        'tests': ['^kernel_.*'],
        'release': ['tot'],
        'days_back': 7,
        },

    'kernel_all': {
        'suites': [
            'kernel_per-build_regression',
            'kernel_per-build_benchmarks',
            'kernel_daily_regression',
            'kernel_daily_benchmarks',
            'kernel_weekly_regression',
            'kernel_weekly_benchmarks',
            ],
        'releases': ['tot'],
        'days_back': 7,
        },

    'kernel_daily': {
        'suites': ['kernel_daily_regression',
                   'kernel_daily_benchmarks',
                   ],
        'releases': ['tot'],
        'days_back': 7,
        },

    'paygen_au_beta': {
        'suites': ['paygen_au_beta'],
        'releases': ['tot'],
        'days_back': 7,
        },

    'paygen_au_canary': {
        'suites': ['paygen_au_canary'],
        'releases': ['tot'],
        'days_back': 4,
        },

    'paygen_au_dev': {
        'suites': ['paygen_au_dev'],
        'releases': ['tot'],
        'days_back': 7,
        },

    'paygen_au_stable': {
        'suites': ['paygen_au_stable'],
        'releases': ['tot'],
        'days_back': 7,
        },

    'power': {
        'suites': ['power_build'],
        'releases': ['tot'],
        'days_back': 3,
        },

    'power_daily': {
        'suites': ['power_daily'],
        'releases': ['tot'],
        'days_back': 7,
        },

    'security': {
        'suites': BVT_SUITES + ['security', 'security_weekly'],
        'releases': ['tot'],
        'days_back': 3,
        'tests': [
            '^security_.*',
            '^platform_DMVerity.*',
            'kernel_ConfigVerify',
            'platform_EncryptedStateful',
            'platform_OSLimits',
            'platform_ToolchainOptions',
            'platform_x86Registers'
            ],
        },

    'simon': {
        'suites': [
            'kernel_per-build_regression',
            'kernel_per-build_benchmarks',
            'power_build',
            'audio',
            ],
        'releases': ['tot'],
        'days_back': 3,
        },

    'simon_daily': {
        'suites': [
            'kernel_daily_regression',
            'kernel_daily_benchmarks',
            'power_daily',
            ],
        'releases': ['tot'],
        'days_back': 7,
        },

    'perf_v2': {
        'suites': ['perf_v2'],
        'releases': ['tot'],
        'days_back': 7,
        },

    'qav': {
        'suites': ['qav'],
        'releases': ['tot'],
        'days_back': 7,
        },

    'qav_all': {
        # Consider test results from all suites.
        'releases': ['tot'],
        'days_back': 7,
        # Test list needs to be in sync with tests that are scheduled by qav.
        # It can be obtained via:
        # ~/trunk/src/third_party/autotest/files/site_utils/suite_enumerator.py
        #     -a ~/trunk/src/third_party/autotest/files qav
        #     | sort | rev | cut -d/ -f2 | rev | uniq
        'tests': [
            'audio_Aplay',
            'audio_AudioCorruption',
            'audio_Microphone',
            'desktopui_MediaAudioFeedback',
            'graphics_GLBench',
            'graphics_GLMark2',
            'graphics_WebGLPerformance',
            'network_VPNConnect',
            'platform_CrosDisksFilesystem',
            'platform_KernelVersionByBoard',
            'power_AudioDetector',
            'power_Idle',
            'video_ChromeHWDecodeUsed',
            'video_VDAPerf',
            'video_VideoCorruption',
            'video_VideoSeek',
            'video_VimeoVideo',
            'video_WebRtcPerf',
            'video_YouTubeFlash',
            'video_YouTubeHTML5',
          ],
        },

    'telemetry': {
        'suites': ['perf_v2'],
        'releases': ['tot'],
        'days_back': 7,
        },

    'video': {
        'suites': BVT_SUITES + ['hwqual', 'perf_v2', 'video'],
        'tests': [
            '^camera_.*',
            '^hardware_Video.*',
            '^telemetry_Benchmarks.media.*',
            '^video_.*',
            ],
        'releases': ['tot'],
        'days_back': 4,
        },

    'wifi_matfunc': {
        'suites': ['wifi_matfunc'],
        'releases': ['tot'],
        },

    'youtube': {
        'suites': [
            'youtube_mse_eme',
            'youtube_page',
            ],
        'releases': ['tot'],
        'days_back': 7,
        },
    }


def default_filter():
    """Return the key of the default named filter when none are supplied."""
    return 'kernel'
